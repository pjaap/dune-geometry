Source: dune-geometry
Section: libs
Priority: optional
Standards-Version: 4.5.0
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Ansgar <ansgar@debian.org>
Vcs-Browser: https://salsa.debian.org/science-team/dune-geometry
Vcs-Git: https://salsa.debian.org/science-team/dune-geometry.git
Homepage: https://www.dune-project.org/
Build-Depends: debhelper-compat (= 13),
 cmake (>= 3.13), gfortran, mpi-default-bin, mpi-default-dev, pkg-config, python3,
 libdune-common-dev (>= 2.8.0)
Build-Depends-Indep: doxygen, ghostscript, graphviz, imagemagick, inkscape, texlive-latex-extra, texlive-latex-recommended, texlive-pictures
Rules-Requires-Root: no

Package: libdune-geometry-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends},
 libdune-common-dev (>= 2.8.0)
Provides: ${dune:shared-library}
Description: toolbox for solving PDEs -- geometry classes (development files)
 DUNE, the Distributed and Unified Numerics Environment is a modular toolbox
 for solving partial differential equations (PDEs) with grid-based methods.
 It supports the easy implementation of methods like Finite Elements (FE),
 Finite Volumes (FV), and also Finite Differences (FD).
 .
 This package contains the development files for the geometry classes.

Package: libdune-geometry-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Built-Using: ${dune:doc:Built-Using}
Description: toolbox for solving PDEs -- geometry classes (documentation)
 DUNE, the Distributed and Unified Numerics Environment is a modular toolbox
 for solving partial differential equations (PDEs) with grid-based methods.
 It supports the easy implementation of methods like Finite Elements (FE),
 Finite Volumes (FV), and also Finite Differences (FD).
 .
 This package contains the documentation for the geometry classes.
